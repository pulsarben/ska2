# Simple tests for predictgp
import os
import pickle
import pytest
import numpy as np
import ska2
import sys

# We store known correct results as "fixtures" in the fixtures directory, these are loaded by tests as needed.
# Some of the data was saved using "pickle" for convenience, e.g.
#     gp_out = gp.predict("mat32")
#     with open("test_predict_gp_mat32.dat", "w") as fil:
#         pickle.dump(gp_out, fil)
# To make the comparison reliable, we pickle the result from the test and compare.

DATA_DIR = os.path.dirname(__file__) + "/fixtures"

def get_gp():
    """ Get a GP object to test with from an example data file """
    res = np.loadtxt(DATA_DIR + "/test_predictgp.dat", unpack=True, usecols=[0,5,6])
    start = min(res[0])
    gp = ska2.PredictRes(res, start, showplot=False, poly_n=2)

    return gp


def predict(kernel):
    """ Test that the predict white mode works """
    with open(DATA_DIR + "/test_predictgp_" + kernel +".dat") as result:
       pickled_ref_data = result.read()

    prediction = get_gp().predict(kernel)
    pickled_prediction = pickle.dumps(prediction)
    return pickled_prediction == pickled_ref_data

def test_judge():
    """ Run the judge funtion and check for the expected result """
    expected_result = (0.0, 9.999999999999997e-06, 1.5632600608533193e-05, 9.8e-06)
    gp = get_gp()
    assert gp.judge() == expected_result

def test_predict_white():
    assert predict("white")

def test_predict_rbf():
    assert predict("rbf")

def test_predict_mat32():
    assert predict("mat32")

def test_predict_invalid_kernel(capsys):
    """ Check an invalid kernel results in a suitable error and exits """
    # See https://docs.pytest.org/en/latest/capture.html
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        get_gp().predict("kjdkfjdkfj")
    captured = capsys.readouterr()
    assert "Unrecognised kernel option" in captured.out
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 9

