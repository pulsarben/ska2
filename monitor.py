#!/usr/bin/env python

from __future__ import print_function
import numpy as np
import os, sys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import datetime

# A module to detect outliers in pulsar timing data
#
# Author: Benjamin Shaw
# Email : benjamin.shaw@manchester.ac.uk
# web   : www.benjaminshaw.me.uk
#
# Documentation: https://ska2.readthedocs.io/en/latest/monitor.html


class Monitor(object):

    """
    **************************************************************************
                                                                            
                         Pulsar timing data monitor
                                                                            
    **************************************************************************

    :param pulsar:      The name of the pulsar whose data is being monitored,
                        (type=str, Default=None) 

    :return:            Monitor object
     
    **Description:**                                                           

    This module will eventually do more things but currently, 
    it takes the output from predictgp.py and computes whether
    or not the latest TOA is consistent with historical residuals.
    In other words it is a simple outlier detector and alerter.

    In order for an outlier to be designated as such, the value of the 
    observed residuals must be more than some distance away from the
    predicted value. This threshold is set by the user.

    ************************************************************************ 
    Author: Benjamin Shaw                                                  

    Email : benjamin.shaw@manchester.ac.uk 

    Web   : http://www.benjaminshaw.me.uk                                  

    ************************************************************************

    **Running the code:**                                                      

    The constructor of the module takes in the name of the pulsar, should it be 
    suppled, else this is just None.

    Import the code by running 

      ``from monitor import Monitor``

    if running as a standalone package or simply

      ``import ska2``

    if running as part of the whole ska2 module.

    We then create the monitor object by typing

      ``mon = Monitor(pulsar="Name of pulsar")``

    One can then pass this object to the test_outlier method

    *************************************************************************
"""

    def __init__(self, pulsar=None):
         
        self.pulsar = pulsar

        self.recipients = ['benjamin.shaw@manchester.ac.uk']

    def test_outlier(self, pres, perr, obsres, obserr, threshold=3.0, autostop=False):
        """ 
        Compares value of latest residuals
        with that predicted by a GP model. 
  
        The method measures the position of a new residual
        with respect to the that predicted at the epoch of 
        the new residuals as informed by a Gaussian process
        trained on all data that does not include the latest
        residual.

        If a TOA is measured to be late or early by more than
        a threshold set by the user (default=3), the an email 
        alert is sent to someone to check and/or a alert is written
        to the console.

        :param pres:      The predicted residuals (type=float)
        :param perr:      The standard deviation at the test locaton (type=float)
        :param obsres:    The actual value of the new residual (type=float)   
        :param obserr:    The error on the new residual (type=float)        
        :param threshold: If the true residual is more than this number of standard deviations
                          away from the predicted value, the residual is an outlier. 
                          (type=float, Default=3)
        :param autostop:  This is for automated monitoring, if the monitor is being used
                          as part of the wider ska2 package. If set to True, monitor will
                          look in a file called auto that exists in the pulsar's timing 
                          directory. If auto is set to ON an alert will be sent, should an
                          outlier be detected and then auto will be switched to OFF. This is to avoid 
                          repeated alerts for the same event. If auto is OFF, a repeat alert will
                          be sent, telling the user that an object for which an alert is already 
                          open, has been reobserved.  Once a user has dealt with an alert, auto 
                          should be reset to ON. 

        :return:  The status of the alert. True if outlier is detected, else False. (type=bool) 

        Run as follows:

            ``status = mon.test_outlier(pres, perr, obsred, obserr)``

        """ 

        self.threshold = threshold
        alert=False

        subj = "Outlier detected in {}".format(self.pulsar)

        # TOA early
        if obsres + obserr < pres - (self.threshold * perr):
            nsig = (obsres + obserr) / perr
            message = "Newest TOA early by {} sigma".format(nsig)
            print(message)
            self._alert(subj, message)
            if autostop:
                self._disable_auto()
            alert = True
            return alert
        # TOA late
        elif obsres - obserr > pres + (self.threshold * perr):
            nsig = (obsres - obserr) / perr
            message = "Newest TOA late by {} sigma".format(nsig)
            print(message)
            self._alert(subj, message)
            if autostop:
                self._disable_auto()
            alert=True
            return alert
        # TOA ok
        else:
            print("No outlier detected")
            return alert

    def repeat_alert(self):
        '''
        Sends a repeat alert if a pulsar is observed after an alert
        for the previous observations has already been sent and 
        not handled by an astronomer.

        Parameters: None
        Return: None
        '''
        message = "New observation of {}. Already open alert. Please check".format(self.pulsar)
        subj = "Reminder! {} has been observed again!".format(self.pulsar)
        self._alert(subj, message)

    def _disable_auto(self): 
        '''
        turns off automatic gp predicting in the event 
        that a outlier is detected
   
        Parameters: None
        Returns: None
        '''
        if self.pulsar:
            autopath = "/local/scratch/bshaw/SKA/pulsars/" + self.pulsar + "/auto"
            now = datetime.datetime.now()
            autoline = "OFF auto " + str(now)
            with open(autopath, 'a') as autofile:
                autofile.write(autoline)
        else:
            print("Will not disable monitoring. No pulsar supplied")

    def _alert(self, subj, message):
        '''
        Send alert

        Parameters: None
        Return: None
        '''
        
        msg = MIMEMultipart()

        msg['From'] = "PulsarDaemon"
        msg['To'] = ' '.join(self.recipients)
        msg['Subject'] = subj

        msg.attach(MIMEText(message, 'plain'))

        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(msg['From'], self.recipients, msg.as_string())         
        print("Alert sent")
