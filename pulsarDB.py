#!/usr/bin/env python

import os
import sys
import csv
from pulsar import Pulsar


class PulsarDB():

    """
    This class reads information from a pulsar's files and 
    returns them as objects.

    The class can read and return a pulsar's:
        ephemeris
        tim file
        the bad MJDs file

    It also constructs a list of pulsars that are in the 
    timing database and checks if a pulsar really exists
         

    """


    def __init__(self, path="/local/scratch/bshaw/SKA/pulsars"):
      
        """
        Constructor

        Parameters: 
            The path to a directory containing subdirectories
            corresponding to each pulsar we are monitoring

        Returns:
            None
        """

        self.path = path
        self._pulsar_list = self._make_pulsar_list(path)

    def _make_pulsar_list(self, path):
        """
        Looks in the top level of the timing directory
        i.e., pulsars/<pulsar> where <pulsar> is a 
        directory corresponding to a pulsar and contains the
        archives, tim file, ephemeris, templates etc for that
        pulsar. 

        Parameters:
            path: Path to the directory containing the pulsars
                  (type=str)

        Returns:
            pulsar_list: List of pulsar names (type=list)
        """
        pulsar_list = []
        dir_list = os.listdir(self.path)
        for item in dir_list:
            if self.validate_pulsar(item):
                pulsar_list.append(item)
        return pulsar_list

    def validate_pulsar(self, item):
        ''' Is the pulsar valid? '''
        # TODO: Implement
        return True

    def get_pulsar(self, name):
        """
        Returns a pulsar from the database.
        
        If a pulsar exists (i.e., it has a directory
        corresponding to the name supplied, then 
        this methods returns the pulsar as an object from which 
        the ephemeris, tim file, and del file can be extracted.

        Parameters:
            name: Pulsar name (type=str)
         
        Returns:
            pulsar: (type=object)
        """
        if self.validate_pulsar(name):
            # Get Pulsar
            ephemeris = self._read_ephemeris(name)
            timing = self._read_timing(name)
            sats_del = self._read_sats_del(name)
            pulsar = Pulsar(ephemeris, timing, sats_del)
        else:
            pulsar = None
        return pulsar

    def _read_timing(self, name):
        """
        Read the TOAs from the pulsar's TIM file
        on disk
        
        Parameters: 
            name: Pulsar name (type=str)

        Returns:
            timing_data: Contents of tim file
            (type=list)
        """
        timing_filename = self.path + '/' + name + '/roach.tim'
        timing_data = []
        with open(timing_filename) as file:
            for line in file:
                timing_data.append(line.strip())
        return timing_data

    def _read_sats_del(self, name):
        """
        Reads the list of bad MJDs in sats.del
        from disk.
        
        Parameters:
            name: Pulsar name (type=str)

        Returns:
            sats_del: list of bad MJDs (type=list)
        """
        sats_del_filename = self.path + '/' + name + '/sats.del'
        sats_del = []
        try:
            with open(sats_del_filename) as file:
                for line in file:
                    sats_del.append(line.strip())
        except IOError:
            sats_del = None
        return sats_del

    def _read_ephemeris(self, name):
        """
        Read ephemeris from disk and returns the
        contents as a dictionary

        Parameters:
            name: Pulsar name (type=str)

        Returns: 
            data: Ephemeris data (type=dict)
            
        """
        pulsar_filename = self.path + '/' + name + '/ephemerides/psr.eph'
        data = {}
        with open(pulsar_filename) as file:
            lines = file.readlines()
            new_lines = []
            for line in lines:
                line = line.rstrip(' \r\n')
                new_lines.append(line)
            reader = csv.reader(new_lines, delimiter=' ',
                                skipinitialspace=True)
            for row in reader:
                data[row[0]] = row[1:]
        return data

    def get_pulsar_list(self):
        ''' Return the Pulsar List '''
        return self._pulsar_list
