#!/usr/bin/env python

# Import modules
from __future__ import print_function
import sys
import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import RBF, WhiteKernel, Matern
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import tempfile

# A module to predict the value of pulsar timing residuals
#
# Author: Benjamin Shaw
# Email : benjamin.shaw@manchester.ac.uk
# web   : www.benjaminshaw.me.uk
#
# Documentation: https://ska2.readthedocs.io/en/latest/predictgp.html

class PredictRes(object):

    """
    **************************************************************************
                                                                            
                       Pulsar timing residual predictor                    
                                                                            
    **************************************************************************

    :param res:         3D array of residuals where
                        res[0] is the MJDs of the residuals,
                        res[1] is the value of the residuals in seconds,
                        res[2] is the uncertainties on the residuals in seconds
                        (type=np.array).
    :param start:      The MJD from which the ephemeris is valid
                        (type=float)
    :param n_restarts:  The number of restarts of the optimizer
                        for finding the kernel's parameters
                        which maximize the log-marginal
                        likelihood (type=int, default=20)
    :param showplot:    Determines whether to show a plot of the residuals
                        and the GP model (useful for debugging - if setting 
                        showplot to True, disable matplotlib.use('Agg'))
                        (type=bool, default=False)
    :param poly_n:      The order of the polynomial to subtract from the
                        residuals time series. 0th order will subtract nothing,
                        1st order will subtract a linear slope, 2nd order
                        will subtract a quadratic etc... (type=int, default=2)

    :return:            Predictor object
     
    **Description:**                                                           
                                                                           
    Takes pulsar timing residuals and uses them to predict                
    the value of a pulsar timing residual at a new observation            
    epoch. The module's primary use case is to detect unusual events      
    in pulsar rotation and/or emission. If a pulse time-of-arrival (TOA)  
    does not conform to that predicted by past observations of a          
    pulsar, then the pulsar may have glitched, undergone a dramatic       
    profile change that caused an early/late TOA to be measured. It's     
    also possible that the pulsar signal was contaminted by radio         
    frequency inteference, or simply not detected.                        
                                                                            
    The module uses both the pulsar timing model AND the pulsar's         
    timing residuals to make a prediction of the value of a residual      
    at a new observation epoch. The module returns the predicted value    
    of a pulsar timing residuals and its 1-sigma uncertainty and also     
    the ACTUAL value of a pulsar timing residuals and its 1-sigma         
    uncertainty. These parameters can then be analysed manually to        
    determine whether the new TOA is an outlier OR be passed to           
    "monitor" which makes this calculation according to some user         
    defined threshold. See                                                
                                                                            
    https://ska2.readthedocs.io/en/latest/monitor.html                    
                                                                            
    for documentation on "monitor"                                        
                                                                            
    The module fits a Gaussian process to pulsar timing residuals         
    and forward predicts to a new observation epoch. The module uses      
    the Gaussian process regressor from scikit-learn. Documentation for   
    this can be found at:                                                 
                                                                            
    https://scikit-learn.org/stable/modules/gaussian_process.html         
                                                                            
    The code also generates a plot showing the pulsar timing residuals,   
    the gaussian process model of the residuals and the new residual.     
                                                                            
    ************************************************************************ 
    Author: Benjamin Shaw                                                  

    Email : benjamin.shaw@manchester.ac.uk 

    Web   : http://www.benjaminshaw.me.uk                                  

    ************************************************************************

    **Running the code:**                                                      

    The constructor of the module takes in a set of timing residuals and
    preprocesses them so that they are ready to be passed to a Gaussian 
    process regression algoritm.

    Import the code by running 

      ``from predictgp import PredictRes``

    if running as a standalone package or simply

      ``import ska2``

    if running as part of the whole ska2 module.

    We then create the predictor object by typing

      ``predictor = PredictRes(res, start)``

    where res is a 3D array of MJDs, residuals and errorbars
    and start is the epoch from which the start the GP regression

    The preprocessing of the timing residuals follows the steps below.

       * Sort the residuals my MJD from earliest to latest.
       * Remove all residuals that occur before START.
       * Remove all except the most recent 101 residuals. If there are 
         fewer than 101, none shall be removed. This is so that 100
         residuals are used to compute the Gaussian process (the most recent
         is not included as this is the epoch we wish to predict at). This 100 
         is somewhat arbitary, I may include this as tunable parameter later.
       * Subtract a nth order polynomial from the residuals.
       * Return an object representing the most recent 100 residuals
         excluding the MJD at which we wish to predict the residual value.

    One can then pass this object to the judge method or the predict 
    method (see method descriptions below)

    *************************************************************************
"""
    # pylint: disable=too-many-instance-attributes

    # Define covariance functions
    K_WHITE = WhiteKernel(noise_level=1, noise_level_bounds=(1e-10, 1e+1))
    K_MATERN = 1.0 * Matern(length_scale=100, nu=1.5) + K_WHITE
    K_RBF = 1.0 * RBF(length_scale=100) + K_WHITE

    # This dictionary contains two values for each kernel
    # 1) The form of the kernel
    # 2) The number of free parameters of the covariance function
    KERNELS = {
        "WHITE":  [K_WHITE, 1.0],
        "MATERN": [K_MATERN, 3.0],
        "RBF":    [K_RBF, 3.0]}

    # This dictionary contains two values for each kernel
    # 1) The prediction of the mean at the epoch of the last observation
    # 2) The 1 sigma spread at the epoch of the last observation
    # These values are initialised as None
    GP_STATS = {
        "WHITE":  [None, None],
        "MATERN": [None, None],
        "RBF":    [None, None]}

    # This dictionary contains 1 value for each kernel
    # 1) The AICc for the model
    AICCS = {
        "WHITE":  [None],
        "MATERN": [None],
        "RBF":    [None]}

    def __init__(self, res, start, n_restarts=20, showplot=False, poly_n=2):

        # Initialise inputs to class
        self.n_restarts = n_restarts
        self.showplot = showplot
        self.poly_n = poly_n

        # Sort residuals
        res = self._sort_res(res)

        # Get START from par file
        # self.start = self._get_start(eph)
        # self.start = self._get_start2(eph)
        self.start = float(start)

        print("Received STARTING epoch")

        # Get residuals starting range
        self.post_start = self._post_start(res, self.start)
        print("Exluded pre-START epochs")

        # Only select last n residuals for gp
        self.last_n = self._count_residuals(self.post_start)
        act_num = len(self.last_n[0])
        print("Getting up to 101 epochs. {} available".format(act_num))

        # Subtract poly_nth order polynomial from data
        if poly_n > 0:
            print("Subtracting polynomial of order {}".format(poly_n))
            self.last_n = self._remove_poly(self.last_n, self.poly_n)

        # Get coordinates of last residuals
        self.last = self._get_lasts(self.last_n)
        print("Last residuals is {}".format(self.last))

        # Get list of residuals without last
        # point for input to gp
        self.gp_in = self._for_gp(self.last_n)
        self.n_sample = len(self.gp_in[0])

    def predict(self, kernel):

        """
        Takes a user defined kernel and fits a GP
        to the residuals. This method is useful if one already
        knows the form of the data. For example if, timing residuals
        are uncorrelated then we would use the white noise kernel.
        

        :param kernel: Name of the kernel we wish to use
                       in the GP fit (type=str)
                       Currently 3 kernel are implemented
                       (see below)

        :return:       4 element tuple of Gaussian process results.
                       This tuple contains 4 parameters (see list below)

        Kernels currently implemented are:

          * White noise kernel (kernel="white")
            For uncorrelated timing residuals (e.g., approximating white noise).
          * Radial basis function (kernel="rbf")
            For smoothly varying, approximately period residuals.
          * Matern class 32 kernel (kernel="mat32")
            For residuals which contain sharp turnover features.

        Run as follows (e.g., for the white kernel):

            ``gp_results = predictor.predict("white")``

        The returned tuple, gp_results,  contains:

          * The predicted mean at the epoch of the last residual
            (type=float)
          * The predicted sigma at the epoch of the last residuals
            (type=float)
          * The value of the last residual
            (type=float)
          * The error on the last residual
            (type=float)


        """
        if kernel == "white":
            kernel = self.K_WHITE
        elif kernel == "rbf":
            kernel = self.K_RBF
        elif kernel == "mat32":
            kernel = self.K_MATERN
        else:
            print("Unrecognised kernel option")
            print("Options: 'white', 'rbf', 'mat32'")
            sys.exit(9)

        # gp_out[0] = Maximum likelihood
        # gp_out[1] = Optimised hyperparameters
        # gp_out[2] = MJDs of prediction locations
        # gp_out[3] = Predicted residual values
        # gp_out[4] = Predicted errorbars
        gp_out = self._fit_gp(self.gp_in, kernel, self.last[0])
        
        self._make_plots(gp_out, self.gp_in, self.last)

        respred = gp_out[3][-1]
        sigpred = gp_out[4][-1]
        resobs = self.last[1]
        sigobs = self.last[2]

        print("Predicted residual is {} +/- {} (1-sigma)".format(respred, sigpred))
        print("Observed residual is {} +/- {} (1-sigma)".format(resobs, sigobs))

        return respred, sigpred, resobs, sigobs

    def judge(self):

        """
        In most cases we don't know the best kernel to 
        use to model the timing residuals. The "judge" method fits
        multiple kernels and evaluates which of the kernels best models 
        the data and returns the results from that kernel.

        :params: None

        :return:       4 element tuple of Gaussian process results.
                       This tuple contains 4 parameters (see list below)

        
        The decision of which kernel best models the data is informed by
        calculating the Corrected Akaike Information Criterion (AICC).
        This is given by:

        .. math::
           AICC = 2k - 2 \ln(L) + [(2k^2 + 2k)/(n - k -1)]

        where:

           * k is the number of hyperparameters in the kernel
           * L is the maximum value of the log marginal likelihood
           * n is the sample size (i.e., the number of residuals in the fit)

        The best fit model is that with the lowest AICC value.

        Run as follows:

            ``gp_results = predictor.judge()``

        The returned tuple, gp_results, contains:

          * The predicted mean at the epoch of the last residual
            (type=float)
          * The predicted sigma at the epoch of the last residuals
            (type=float)
          * The value of the last residual
            (type=float)
          * The error on the last residual
            (type=float)
        """
        test_data = {
            "WHITE":  None,
            "MATERN": None,
            "RBF":    None}

        for key in self.KERNELS:
            # Fit a GP
            gp_out = self._fit_gp(
                self.gp_in,
                self.KERNELS[key][0],
                self.last[0])
            test_data[key] = gp_out

            # Calculate AICc
            aicc = self._calc_akaike(
                gp_out,
                self.KERNELS[key][1],
                self.n_sample)

            self.AICCS[key] = float(aicc)

            self.GP_STATS[key][0] = gp_out[3][-1]
            self.GP_STATS[key][1] = gp_out[4][-1]

        # Find kernel with lowest AICc score and return
        # statistics
        best_kernel = min(self.AICCS, key=self.AICCS.get)
        print("Using {} kernel".format(best_kernel))

        self._make_plots(test_data[best_kernel], self.gp_in, self.last)
 
        # The predicted value of the residual at the test epoch
        respred = self.GP_STATS[best_kernel][0]
        # The standard deviation on the predicted residual
        sigpred = self.GP_STATS[best_kernel][1]
        # The observed value of the new residual
        resobs = self.last[1]
        # The 1-sigma errorbar on the new residual
        sigobs = self.last[2]

        print("Predicted residual is {} +/- {} (1-sigma)".format(respred, sigpred))
        print("Observed residual is {} +/- {} (1-sigma)".format(resobs, sigobs))
        
        return respred, sigpred, resobs, sigobs

    @staticmethod
    def _sort_res(data):
        '''
        Sorts the residuals by order of MJD.
        Typically they would be passed into the class
        in time order anyway, but this may not be the case
        if data from multiple telescopes has been combined.

        Parameters:
           data: 3D array of residuals comprising. The array shape is
                 3*(the number of observations).
                 data[0] is the MJDs of the residuals
                 data[1] is the value of the residuals in seconds
                 data[2] is the uncertainties on the residuals in seconds
                 (type=np.array)

        Returns:
                The same array but sorted by the first column
                (type=ndarray)
        '''
        res = np.asarray(data)
        res = np.transpose(res)
        res = sorted(res, key=lambda x: x[0])
        res = np.transpose(res)

        return res

    @staticmethod
    def _post_start(data, start):
        '''
        Gets rid of all residuals that are earlier than
        the START MJD. This is to ensure that only data
        corresponding to the ephemeris are used. Prior data
        may be described by a different ephemeris.

        Parameters:
            data: 3D array of residuals comprising
                  data[0] is the MJDs of the residuals
                  data[1] is the value of the residuals in seconds
                  data[2] is the uncertainties on the residuals in seconds
                  (type=np.array)

            start: MJD of start of ephemeris (type=float)

        Returns:
           mjd_mod: The list of MJDs that occurred after start
                    (type=list)
           res_mod: The list of residuals after start (units=s)
                    (type=list)
           err_mod: The list of residuals errors after start (units=s)
                    (type=list)
        '''

        # Separate MJD, res, res err into separate arrays
        mjd = data[0]
        res = data[1]
        err = data[2]

        # Create new bins for the modified arrays
        mjd_mod = []
        res_mod = []
        err_mod = []

        # Go through each of the mjds and test whether or
        # not it occurs before start. If it does not,
        # append it to the new array
        for i in range(0, len(mjd)):
            if mjd[i] >= start:
                mjd_mod.append(mjd[i])
                res_mod.append(res[i])
                err_mod.append(err[i])

        return mjd_mod, res_mod, err_mod

    @staticmethod
    def _remove_poly(data, poly_n):
        """
        Subtracts a nth order polynomial
        from a time series. In the case a series
        of time ordered residuals.

        Parameters:
           data: 3D array of residuals comprising
                  data[0] is the MJDs of the residuals
                  data[1] is the value of the residuals in seconds
                  data[2] is the uncertainties on the residuals in seconds
                  (type=np.array)
           poly_n: Order of polynomial to subtract (type=int)

        Returns:
            mjd: List of MJDs (type=list)
            res - pfit(mjd):  List of residuals in seconds (type=list)
            err: List of errorbars in seconds (type=list)
        """

        mjd = data[0]
        res = data[1]
        err = data[2]

        inv_err = []
        for i in range(0, len(err)):
            inv_err.append(1.0 / err[i])

        pfit = np.poly1d(np.polyfit(mjd, res, poly_n, w=inv_err))

        return mjd, res - pfit(mjd), err

    @staticmethod
    def _count_residuals(data):
        """
        Takes a 3D array or residuals and
        returns only the last 101.

        Parameters:
            data: 3D array of residuals
                  data[0] is the MJDs of the residuals
                  data[1] is the value of the residuals in seconds
                  data[2] is the uncertainties on the residuals in seconds
                  (type=np.array)

        Returns:
             mjd: A list of MJDs (type=list)
             res: A list of residuals in seconds (type=list)
             err: A list of errorbars in seconds (type=list)
        """

        mjd = data[0]
        res = data[1]
        err = data[2]

        excess = len(mjd) - 101
        if excess > 0:
            mjd_cut = mjd[excess:]
            res_cut = res[excess:]
            err_cut = err[excess:]
            return mjd_cut, res_cut, err_cut
        else:
            return mjd, res, err

    @staticmethod
    def _get_lasts(data):
        """
        Takes a 3D array (of residuals)
        and chops off the last element of
        each subarray. The last element of 
        each subarray is returned. This 
        corresponds to the new observation's
        MJD, value and error.

        Parameters:
           data:  3D array of residuals
                  data[0] is the MJDs of the residuals
                  data[1] is the value of the residuals in seconds
                  data[2] is the uncertainties on the residuals in seconds
                  (type=np.array)

        Returns:
           last_mjd: The most recent MJD (type=float)
           last_res: The residual (in seconds)
                     corresponding to the most recent
                     MJD (type=float)
           last_err: The error on the most recent
                     residual in seconds (type=float)
        """

        last_mjd = data[0][-1]
        last_res = data[1][-1]
        last_err = data[2][-1]

        return last_mjd, last_res, last_err

    @staticmethod
    def _for_gp(data):
        """
        Takes a 3D array (of residuals)
        and chops off the last element of
        each subarray. This corresponds to the
        MJD of the most recent residuals, the most
        recent residual and its errorbar

        Parameters:
           data:  3D array of residuals
                  data[0] is the MJDs of the residuals
                  data[1] is the value of the residuals in seconds
                  data[2] is the uncertainties on the residuals in seconds
                  (type=np.array)

        Returns:
            mjdsl: A list of MJDs (type=list)
            ressl: A list of residuals (in secs)
                   (type=list)
            errsl: A list of errors on the residuals
                   (type=list)
        """

        mjdsl = data[0][:-1]
        ressl = data[1][:-1]
        errsl = data[2][:-1]

        return mjdsl, ressl, errsl

    def _fit_gp(self, data, kernel, last):
        '''
        Actually does the gp fitting using
        scikit-learn's Gaussian Processes
        library

        Parameters:
           data:     3D array of residuals
                     data[0] is the MJDs of the residuals
                     data[1] is the value of the residuals in seconds
                     data[2] is the uncertainties on the residuals in seconds
                     (type=np.array)
           kernel:   Name of kernel to use (type=str)
           last:     The MJD of the last residuals (i.e., the epoch
                     at which to predict the residual's value)
                     (type=float)

        Returns:
           maxlik:   The maximum likelihood value (type=float)
           opt_hyp:  The optimised hyperparamters (type=class)
           mjdinfer: The MJDs at which a prediction is made
                     (type=numpy.ndarray)
           res_mean: The predicted residuals (type=numpy.ndarray)
           sigma:    The 1 sigma error on the predicted residuals
                     (type=numpy.ndarray)
        '''
        mjd = data[0]
        res = data[1]

        mjd = np.reshape(mjd, (len(mjd), 1))

        kernel = kernel
        print("\nInitialising kernel:\n{}".format(kernel))

        gp_model = GPR(
            kernel=kernel,
            alpha=0.0,
            n_restarts_optimizer=self.n_restarts
            ).fit(mjd, res)

        maxlik = gp_model.log_marginal_likelihood(gp_model.kernel_.theta)
        print("Max log_likelihood: {}".format(maxlik))
        opt_hyp = gp_model.kernel_
        print("Optimised parameters:\n{}".format(opt_hyp))

        mjdinfer = np.append(mjd, [[last]], axis=0)
        mjdinfer = np.reshape(mjdinfer, len(mjdinfer), 0)

        res_mean, res_cov = gp_model.predict(
            mjdinfer[:, np.newaxis],
            return_cov=True)
        sig = 1.0 * np.sqrt(np.diag(res_cov))
        return maxlik, opt_hyp, mjdinfer, res_mean, sig

    @staticmethod
    def _calc_akaike(pars, npars, nsample):
        '''
        Calculate the Akaike Info Criterion
        for a model:
        See https://en.wikipedia.org/wiki/Akaike_information_criterion
        for details of the algorithm

        Parameters:
            pars:    The parameters from gp_out of which the zeroth element
                     is the maximum likelihood (type=class)
            npars:   The number of free parameters in the covariance
                     function (type=float) (should probably be int)
            nsample: Number of data points in the fit (type=int)

        Returns:
            aicc: The value of the information criterion (type=float)
        '''
        aic = 2*npars - 2*pars[0]
        aicc = aic + ((2*npars**2.0 + 2*npars)/(nsample - npars - 1))

        print("AICC:{}".format(aicc))

        return aicc

    #@staticmethod
    #def _make_plots(gp_out, data, last):
    def _make_plots(self, gp_out, data, last):
        '''
        Makes plots of the GP fit
        for each kernel

        Parameters:
            gp_out: The output from the GP predictor. This comprises:
                    1) The maximum likelihood (type=float)
                    2) The optimised hyperparameters (type=class)
                    3) The list if MJDs at which a prediction is made
                       (type=numpy.ndarray)
                    4) The list of predicted residuals (type=numpy.ndarray)
                    5) The list of error on each of the predicted residuals
                       (type=numpy.ndarray)

        Returns:
             None
        '''

        maxlik = gp_out[0]
        opthyp = gp_out[1]
        mjdinfer = gp_out[2]
        res_mean = gp_out[3]
        sigma = gp_out[4]

        mjd = data[0]
        res = data[1]
        err = data[2]

        (dummy, filename) = tempfile.mkstemp()
        write_plot = filename + ".png"

        plt.plot(
            mjdinfer,
            res_mean,
            color='k',
            linestyle='dashed',
            label="GP Model")
        plt.fill_between(
            mjdinfer,
            res_mean - 3.0 * sigma,
            res_mean + 3.0 * sigma,
            alpha=0.5,
            color='k')
        plt.errorbar(
            mjd, res,
            yerr=err,
            marker='.',
            color='r',
            ecolor='r',
            linestyle='None',
            label="Data")
        plt.errorbar(
            last[0], last[1],
            yerr=last[2],
            marker='o',
            color='g',
            ecolor='g',
            linestyle='None', 
            label="New residual")
        plt.title("{}\nLog-Marg-Lik: {}".format(opthyp, maxlik))
        plt.xlabel("MJD", fontsize=20)
        plt.ylabel("Residual [s]", fontsize=20)
        plt.legend(loc=0)
        plt.tight_layout()
        plt.savefig(write_plot, format='png', dpi=400)
        print("Plot written to {}".format(write_plot))
        if self.showplot:    
            plt.show()
        plt.close()
