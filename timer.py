#!/usr/bin/env python

#Import modules
from __future__ import print_function
import sys
import os
import numpy as np
import psrchive

class Timer(object):

    '''
    Currently just forms a TOA from a scrunched 
    archive. Can expand this to do other stuff later
    '''

    def __init__(self, archive, pulsar, timfile="roach.tim"):
        '''
        Initialised paths for the tim file, archive and template
        '''

        self.pulsar = pulsar
        self.archive = archive
        self.timfile = timfile

        self.template = self._select_template(self.pulsar)
        self.tim = self._select_tim(self.pulsar, self.timfile)

        self._check_file(self.archive)
        self._check_file(self.template)
        self._check_file(self.tim)

        identifier = self._get_identifier(self.archive)

    @staticmethod
    def _check_file(thisfile):
        '''
        Checks if critical files exist.
        Terminates the program if they don't
        '''
        if os.path.isfile(thisfile):
            print("Located {}".format(thisfile))
            return True
        else:
            print("{} not found - cannot proceed".format(thisfile))
            sys.exit(9)
        
    @staticmethod
    def _select_template(pulsar):
        '''
        Forms path to template
        '''
        template = "/local/scratch/bshaw/SKA/pulsars/" + pulsar + "/templates/1520.std"
        return template

    @staticmethod
    def _select_tim(pulsar, timfile):
        '''
        Forms path to tim file
        '''
        tim = "/local/scratch/bshaw/SKA/pulsars/" + pulsar + "/" + timfile
        return tim 

    @staticmethod
    def _get_identifier(archive):
        '''
        Gets time identifier of archive
        '''
        filename = str(os.path.split(archive)[1])
        obsdate = os.path.splitext(filename)[0].split('_')[0]
        obstime = os.path.splitext(filename)[0].split('_')[1]
        identifier = obsdate + "_" + obstime
        
        return identifier

    def calc_toa(self):
        '''
        Calculates SAT and a bunch of metadata
        using psrchive python wrapper
        '''

        print("Calculating TOA")

        arrtim = psrchive.ArrivalTime()
        arrtim.set_shift_estimator('SIS')
        arrtim.set_format('Tempo2')
        arrtim.set_format_flags('IPTA')

        std = psrchive.Archive_load(str(self.template))
        std.pscrunch()
        arrtim.set_standard(std)

        obs = psrchive.Archive_load(str(self.archive))
        obs.pscrunch()
        arrtim.set_observation(obs)
   
        timline = arrtim.get_toas()[0]
        return timline
     
    def append_tim(self, timline):
        '''
        Appends new TOA and associated metadata
        to existing roach.tim file
        '''

        print("Appending TOA to {}".format(self.tim))
        with open(self.tim, 'a') as otim:
            otim.write(timline + "\n")


